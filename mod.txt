{
	"name": "MiniMap",
	"description": "Minimap showing player, team and marked enemies for heists with a preplanning map and a subset of other maps with custom minimap textures",
	"blt_version": 2,
		"hooks": [
		{ "hook_id": "lib/managers/hudmanagerpd2", "script_path": "MiniMap.lua" },
		{ "hook_id": "lib/managers/criminalsmanager", "script_path": "MiniMap.lua" },
		{ "hook_id": "lib/units/beings/player/huskplayermovement", "script_path": "MiniMap.lua" },
		{ "hook_id": "lib/units/contourext", "script_path": "MiniMap.lua" },
		{ "hook_id": "lib/managers/hudmanager", "script_path": "MiniMap.lua" }
	]
}